#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>
#include <locale.h>
#include <assert.h>

#include "hash.h"
#include "metodos2.h"

typedef struct {
    wchar_t* palabra;
    int largo;
} word;



void imprimir(void *dato, FILE* archivo){
    if (dato) {
        word* palabra = (word*) dato;
        fwprintf(archivo, L"%ls \n", palabra->palabra);
    }else{
        fprintf(archivo, "\n");
    }
}


unsigned fun_hash(void* palabra){
    unsigned hash = 0x811c9dc5;
    word* aux = (word*) palabra;

    for (int i = 0; i < aux->largo; i++) {
        hash = hash ^ aux->palabra[i];
        hash = hash * 16777619;
    }


    return hash;

}

/**
fun_igualdad: void*, void* -> int
Recibe 2 estructuras word como void*, devuelve 1 si las palabras
son iguales o 0 si no lo son.
*/
int fun_igualdad(void* palabra1, void* palabra2){

    word *aux1, *aux2;
    aux1 = (word*) palabra1;
    aux2 = (word*) palabra2;

    if (aux1->largo == aux2->largo)
        return(!wcscmp(aux1->palabra, aux2->palabra));

    return 0;
}

/**
borrar_palabra: void*
Recibe como parametro una estructura word, libera la memoria de la
palabra y luego de la estructura.
*/
void borrar_palabra(void* palabra) {
    word *aux;
    aux = (word*) palabra;

    free(aux->palabra);
    free(aux);
}

/**
calcular_cantidad: char* -> int
Recibe el nombre de un archivo como parametro y devuelve
la cantidad de palabras que tiene.
*/

int calcular_cantidad(char nombreArchivo[]){  //cuenta bien
    int contador = 0;
    FILE* archivo;
    char aux[25];
    archivo = fopen(nombreArchivo, "r");
    while (fscanf(archivo,"%s", aux) == 1){  //por algun motivo el !feof no me anda mas asique lo hice asi xd
        contador++;
    }

    fclose(archivo);
    return contador;

}

/**
archivo_valido: char*
Recibe el nombre de un archivo como parametro, devuelve 1 si el archivo
existe/puede abrirlo y no esta vacio. En caso contrario devuelve 0.
*/

int archivo_valido(char nombreArchivo[]){
    FILE* archivo;
    char aux;
    archivo = fopen(nombreArchivo, "r");

    if (archivo != NULL){
        aux = fgetc (archivo);
        fclose(archivo);

        if (aux != EOF) {
            return 1;       // Existe y no vacio
        }
    }
    return 0;             // Esta vacio o no existe
}
/**
minuscula: wchar_t*, wchar_t*, int
Recibe como parametros una palabra, un array para guardar el resultado y el largo
de la palabra. Pasa a minuscula cada caracter de la palabra y los copio en el array
de resultado.
*/
void minuscula (wchar_t nuevaPalabra[], wchar_t palabra[]) {
    int i;
    for (i = 0; palabra[i] != L'\0'; i++){
        nuevaPalabra[i] = wtolower(palabra[i]);
    }
    nuevaPalabra[i] = palabra[i];
}

/**
insertarPalabra: TablaHash*, wchar_t*
Recibe como parametro un puntero a estructura tabla hash y una palabra. Crea
una estructura word, guarda la palabra, su largo y llama a la funcion
que se encarga de insertar la estructura en la tabla.
*/
void insertarPalabra (TablaHash* tabla, wchar_t aux[]){
    int largo = wcslen(aux);
    word* palabra = malloc(sizeof(word));

    palabra->palabra = malloc(sizeof(wchar_t)*(largo+1));
    minuscula(palabra->palabra, aux);
    palabra->largo = largo;

    tablahash_insertar(tabla, palabra);
}

/**
insertar: char*, TablaHash*, int
Recibe como parametro el nombre de un archivo, un puntero a estructura tabla hash y la cantidad
de palabras de nuestro universo. Abre el archivo, y por cada palabra leida llama a la
funcion que se encarga de insertarla en la tabla. Finalmente cierra el archivo.
*/
void insertar (char nombreArchivo[], TablaHash* tabla, int cantidadPalabras){
    FILE* archivo;
    archivo = fopen(nombreArchivo, "r");
    wchar_t aux[40];
    for (int i = 0; i < cantidadPalabras; i++){
        fwscanf(archivo, L"%ls", aux);
        insertarPalabra(tabla, aux);
    }
    fclose(archivo);
}

/** auxiliar
imprimir_palabars: void*, FILE*

*/
void imprimir_palabras (void* dato, FILE* archivo) {
    wchar_t* palabra = (wchar_t*) dato;
    fwprintf(archivo, L" %ls\n", palabra);
}

/**
agregar_sugerencias: GList, TablaHash*, wchar_t*, int
Recibe como parametro una GList, un puntero a estructura tabla hash, una palabra y su largo.
Si la palabra palabra se encuentra en la tabla y no se encuentra en la lista correspondiente
a su clave, se agrega al final de esa lista. En caso contrario, no se agrega.
*/
void agregar_sugerencia(GList sugerencias, TablaHash* tabla, wchar_t* palabra, int largo){
    word pal;
    pal.palabra = palabra;
    pal.largo = largo;

    if(tablahash_buscar(tabla, &pal)){

        if(!GList_buscar(sugerencias, fun_igualdad, &pal)){

            word* palabra1 = malloc(sizeof(word));
            palabra1->palabra = malloc(sizeof(wchar_t)*(largo+1));
            palabra1->largo = largo;
            wcscpy(palabra1->palabra, palabra);

            GList_agregar_final(sugerencias, palabra1);
        }
    }
}

/**
revisar_palabra_separada: GList, TablaHash*, wchar_t, int, wchar_t, int

*/
void revisar_palabra_separada(GList sugerencias, TablaHash* tabla, wchar_t* palabra, int largo, int posEspacio){
    word pal, pal1, pal2;
    wchar_t palabra1[largo], palabra2[largo];
    pal.largo = largo;
    pal.palabra = palabra;

    pal1.palabra = palabra1;
    pal1.largo = posEspacio;

    pal2.palabra = palabra2;
    pal2.largo = largo-posEspacio-1;


    wcsncpy(pal1.palabra, palabra, posEspacio);
    pal1.palabra[posEspacio] = 0;

    for (int i = posEspacio+1, j = 0; i <= largo; i++, j++) { //<= para que copie el terminador
        pal2.palabra[j] = palabra[i];
    }


    if(tablahash_buscar(tabla, &pal1) && tablahash_buscar(tabla, &pal2)){

        if(!GList_buscar(sugerencias, fun_igualdad, &pal)){

            word* palabra1 = malloc(sizeof(word));
            palabra1->palabra = malloc(sizeof(wchar_t)*(largo+1));
            palabra1->largo = largo;
            wcscpy(palabra1->palabra, palabra);

            GList_agregar_final(sugerencias, palabra1);
        }
    }
}

/**
guardar_resultado: GList, wchar_t, int
Recibe como parametro una GList, una palabra y su largo. Pide memoria para una estructura
word y la palabra, guarda la palabra con su largo en la estructura y finalmente la agrega
al final de la lista reciba como parametro.

*/
void guardar_resultado(GList lista, wchar_t* palabra, int largo){
    word* pal = malloc(sizeof(word));
    pal->palabra = malloc(sizeof(wchar_t)*(largo+1));

    wcscpy(pal->palabra, palabra);
    pal->largo = largo;

    GList_agregar_final(lista, pal);

}

/**
generar_sugerencias: word*, TablaHash*, wchar_t, int, GList, int, GList

*/
void generar_sugerencias(word* palabra, TablaHash* tabla, wchar_t* alfabeto, int largo_alfabeto,
                         GList sugerencias, int guardar_resultados, GList resultados, int nivel1){
    int largo;
    wchar_t *pal, *palabra_separada, *palabra_intercambiada, *palabra_eliminada, *palabra_agregada, *palabra_reemplazada;

    largo = palabra->largo;
    pal = palabra->palabra;

    wchar_t palabras_generadas[5][largo+2];

    palabra_separada =      palabras_generadas[0];
    palabra_intercambiada = palabras_generadas[1];
    palabra_eliminada =     palabras_generadas[2];
    palabra_agregada =      palabras_generadas[3];
    palabra_reemplazada =   palabras_generadas[4];

    palabra_eliminada[0] = 0;

    //Inicializacion de palabra intercambiada y palabra reemplazada
    wcscpy(palabra_intercambiada, pal);
    wcscpy(palabra_reemplazada, pal);
    wcscpy(palabra_separada, pal);
    palabra_separada[largo+1] = 0;


//Se aplica la tecnica de eliminacion en todas las posiciones, y se revisa si el resultado
//se encuenta en el diccionario
    for (int i = 0; i < largo && (sugerencias->cantidad_elementos < 5 || nivel1); i++){
        eliminacion(pal, largo, i, palabra_eliminada);
        agregar_sugerencia(sugerencias, tabla, palabra_eliminada, largo-1);

        if (guardar_resultados)
            guardar_resultado(resultados, palabra_eliminada, largo-1);
    }

    for (int i = 1; i < largo && (sugerencias->cantidad_elementos < 5 || nivel1); i++){
        if (palabra_intercambiada[i] != pal[i-1]) {
            intercambio(pal, largo, i, palabra_intercambiada);

            agregar_sugerencia(sugerencias, tabla, palabra_intercambiada, largo);
            if (guardar_resultados)
                guardar_resultado(resultados, palabra_intercambiada, largo);
        } else {
            if (i > 1)
                palabra_intercambiada[i-2] = pal[i-2];
        }
    }

    for (int i = 1; i < largo && sugerencias->cantidad_elementos < 5; i++){
        separar_palabra(pal, largo+1, i, palabra_separada);

        revisar_palabra_separada(sugerencias, tabla, palabra_separada, largo+1, i);
    }

    for (int i = 0; i < largo && (sugerencias->cantidad_elementos < 5 || nivel1); i++){
        for(int j = 0; j < largo_alfabeto && (sugerencias->cantidad_elementos < 5 || nivel1); j++){
            reemplazo (pal, largo, palabra_reemplazada, i, alfabeto[j]);

            agregar_sugerencia(sugerencias, tabla, palabra_reemplazada, largo);

            if (guardar_resultados)
                guardar_resultado(resultados, palabra_reemplazada, largo);
        }
    }

    for (int i = 0; i <= largo && (sugerencias->cantidad_elementos < 5 || nivel1); i++){
        for(int j = 0; j < largo_alfabeto && (sugerencias->cantidad_elementos < 5 || nivel1); j++) {
            agregar_entre_letras(pal, palabra_agregada, largo+1, i, alfabeto[j]);

            agregar_sugerencia(sugerencias, tabla, palabra_agregada, largo+1);

            if (guardar_resultados)
                guardar_resultado(resultados, palabra_agregada, largo+1);
        }
    }
}

/**
preparar_palabra: word*, wchar_t*, int
Recibe como parametro un puntero a estructura word, una palabra y su largo.
Guarda en la estrutura la palabra y su largo.
*/
void preparar_palabra(word* pal, wchar_t* palabra, int largo){
    pal->palabra = palabra;
    pal->largo = largo;
}

/**
explorar: word*, TablaHash*, wchar_t*, int, GList, int

*/
void explorar(word* palabra, TablaHash* tabla, wchar_t* alfabeto, int largo_alfabeto,
              GList sugerencias, int nivel){

    if (nivel == 1)
        generar_sugerencias(palabra, tabla, alfabeto, largo_alfabeto, sugerencias, 0, NULL, 0);

     else{
        int largo;
        wchar_t *pal, *palabra_intercambiada, *palabra_eliminada, *palabra_agregada, *palabra_reemplazada;
        word palabra_actual;

        largo = palabra->largo;
        pal = palabra->palabra;

        wchar_t palabras_generadas[6][largo+2];

        palabra_intercambiada = palabras_generadas[2];
        palabra_eliminada =     palabras_generadas[3];
        palabra_agregada =      palabras_generadas[4];
        palabra_reemplazada =   palabras_generadas[5];

        palabra_eliminada[0] = 0;

        //Inicializacion de palabra intercambiada y palabra reemplazada
        wcscpy(palabra_intercambiada, pal);
        wcscpy(palabra_reemplazada, pal);



        for (int i = 0; i < largo && sugerencias->cantidad_elementos < 5; i++){
            eliminacion(pal, largo, i, palabra_eliminada);

            preparar_palabra(&palabra_actual, palabra_eliminada, largo-1);
            explorar(&palabra_actual, tabla, alfabeto, largo_alfabeto, sugerencias, nivel-1);
        }

        for (int i = 1; i < largo && sugerencias->cantidad_elementos < 5; i++){
            intercambio(pal, largo, i, palabra_intercambiada);

            preparar_palabra(&palabra_actual, palabra_intercambiada, largo);
            explorar(&palabra_actual, tabla, alfabeto, largo_alfabeto, sugerencias, nivel-1);
        }


        for (int i = 0; i < largo && sugerencias->cantidad_elementos < 5; i++){
            for(int j = 0; j < largo_alfabeto && sugerencias->cantidad_elementos < 5; j++){
                reemplazo (pal, largo, palabra_reemplazada, i, alfabeto[j]);

                preparar_palabra(&palabra_actual, palabra_reemplazada, largo);
                explorar(&palabra_actual, tabla, alfabeto, largo_alfabeto, sugerencias, nivel-1);
            }
        }

        for (int i = 0; i <= largo && sugerencias->cantidad_elementos < 5; i++){
            for(int j = 0; j < largo_alfabeto && sugerencias->cantidad_elementos < 5; j++) {
                agregar_entre_letras(pal, palabra_agregada, largo+1, i, alfabeto[j]);
                preparar_palabra(&palabra_actual, palabra_agregada, largo+1);
                explorar(&palabra_actual, tabla, alfabeto, largo_alfabeto, sugerencias, nivel-1);
            }
        }

    }
}

/**
buscar_sugerencias: word*, TablaHash*, wchar_t*, int, GList, GNodoP
Recibe como parametro un puntero a estructura word, un puntero a estructura tabla hash, el alfabeto, el largo del alfabeto
y una GList para guardar las sugerencias. Aplica las 5 tecnicas a la palabra recibida como parametro, si no se consiguen
las 5 sugerencias, se combinan las tecnicas hasta obtener 5 sugerencias.
*/
void buscar_sugerencias(word* palabra, TablaHash* tabla, wchar_t* alfabeto, int largo_alfabeto, GList sugerencias){
    GNodoP nodo;
    GList primer_nivel = GList_crear();

    generar_sugerencias(palabra, tabla, alfabeto, wcslen(alfabeto), sugerencias, 1, primer_nivel, 1);

    if(sugerencias->cantidad_elementos < 5){
        for(int i = 1; sugerencias->cantidad_elementos < 5; i++){
            for(nodo = primer_nivel->principio; nodo && sugerencias->cantidad_elementos < 5; nodo = nodo->sig){
                explorar(nodo->dato, tabla, alfabeto, largo_alfabeto, sugerencias, i);
            }
        }

    }

    GList_destruir(primer_nivel, &borrar_palabra);
}

void output (wchar_t* palabra, int linea, GList sugerencias, FILE* archivoSalida){
    fwprintf (archivoSalida, L"Linea %d, %ls no esta en el diccionario.\nQuizas quiso decir: ", linea, palabra);

    GNodoP nodo;
    nodo = sugerencias->principio;
    word* palabra_sugerida;

    for (; nodo;) {
        palabra_sugerida = (word*) nodo->dato;
        fwprintf(archivoSalida,L"'%ls '", palabra_sugerida->palabra);
        nodo = nodo->sig;
    }

    fwprintf (archivoSalida, L"\n\n");

}

void parsear_palabras (wchar_t aux[], int contadorLinea, FILE* archivoSalida, TablaHash* tabla, wchar_t* alfabeto) {
    if (aux[0] != '\n'){
        GList sugerencias;
        word palabra;
        wchar_t pal_minuscula[50];
        wchar_t buffer[50];
        int contadorBuffer = 0, largoLinea = wcslen(aux);

        for (int i = 0; i < largoLinea; i++){
            if (aux[i] != ' ' && aux[i] != '\n' && aux[i] != '\r'){
                buffer[contadorBuffer] = aux[i];
                contadorBuffer++;
            } else {
                if (aux[i-1] <= 64){
                    buffer[contadorBuffer - 1] = 0;
                } else {
                    buffer[contadorBuffer] = 0;
                }

                contadorBuffer = 0;

                minuscula(pal_minuscula, buffer);
                palabra.palabra = pal_minuscula;
                palabra.largo = wcslen(buffer);

                if(!tablahash_buscar(tabla, &palabra)){
                    sugerencias = GList_crear();
                    buscar_sugerencias(&palabra, tabla, alfabeto, wcslen(alfabeto), sugerencias);
                    output(buffer, contadorLinea, sugerencias, archivoSalida);
                    GList_destruir(sugerencias, &borrar_palabra);
                }

            }

        }
    }

}

void generar_solucion (char nombreArchivoACorregir[], TablaHash* tabla, wchar_t* alfabeto){
    char nombreArchivoSalida[25];
    wchar_t aux[100];
    int contadorLinea = 1;
    FILE* archivoSalida, *archivoACorregir;

    printf("Ingrese el nombre del archivo de salida: \n");
    scanf("%s", nombreArchivoSalida);

    archivoACorregir = fopen(nombreArchivoACorregir, "r");
    archivoSalida = fopen(nombreArchivoSalida, "w");

    while (fgetws (aux , 100 , archivoACorregir) != NULL ){
        parsear_palabras(aux, contadorLinea, archivoSalida, tabla, alfabeto);
        contadorLinea++;
    }

    fclose(archivoACorregir);
    fclose(archivoSalida);

}

int main() {
    char nombreArchivo[25], nombreArchivoACorregir[25];
    int cantidadPalabras;
    wchar_t alfabeto[35] = L"abcdefghijklmnopqrstuvwxyzáéíóúäöü";
    TablaHash* tabla;
    //GList sugerencias;

    setlocale(LC_CTYPE,"");
    //fwide(stdout, 1);

    printf("Ingrese nombre del archivo con el lemario: \n");
    scanf("%s", nombreArchivo);

    while (archivo_valido(nombreArchivo) != 1){
        printf("El archivo no existe o no se puede abrir.\nIngrese nuevamente el nombre del archivo: \n");
        scanf("%s", nombreArchivo);
    }

    cantidadPalabras = calcular_cantidad(nombreArchivo);

    tabla = tablahash_crear(cantidadPalabras, &fun_hash, &fun_igualdad, &borrar_palabra);

    insertar(nombreArchivo, tabla, cantidadPalabras);  //Nose si queres pasarle la tabla hash de parametro o que la haga adentro

    //primer_nivel = GList_crear();

    printf("Ingrese nombre del archivo a corregir: \n");
    scanf("%s", nombreArchivoACorregir);

    while (archivo_valido(nombreArchivoACorregir) != 1){
        printf("El archivo no existe o no se puede abrir.\nIngrese nuevamente el nombre del archivo: \n");
        scanf("%s", nombreArchivoACorregir);
    }

    generar_solucion(nombreArchivoACorregir, tabla, alfabeto);

    //buscar_sugerencias(palabra, tabla, alfabeto, wcslen(alfabeto), sugerencias);
    //tablahash_diagnostico(tabla, &imprimir_palabras);
    tablahash_destruir(tabla);

    //GList_imprimir(sugerencias, "listasugerencias", &imprimir);
    //GList_imprimir(primer_nivel, "nivel1", &imprimir);

    //GList_destruir(sugerencias, &borrar_palabra);
    //GList_destruir(primer_nivel, &borrar_palabra);
    //borrar_palabra(palabra);

    return 0;
}
