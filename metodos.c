//Funciones para encontrar sugerencias
/*
La dejo por las dudas pero F
//26 minusculas/mayusculas + 8 raras mayuscula/minuscula
void rellenar_diccionario (int diccionario[][34], wchar_t alfabeto[]){
    int indice = 0, fila = 0, letra = 0, flag = 0;
    while (alfabeto[letra]){
        if (indice == 34){
            fila = 1;
            indice = 0;
        } else {
            for (int ascii = 0; ascii <= 252 && !flag; ascii++){
                if (ascii == alfabeto[letra]){
                    diccionario[fila][indice] = ascii;
                    wprintf(L"Letra: %lc - ASCII: %d\n", alfabeto[letra], diccionario[fila][indice]);
                }
            }
        }
        letra++;
    }
}
*/

wchar_t* a_minuscula (wchar_t palabra[]) {
    int largoPalabra = wcslen(palabra);
    wchar_t* nuevaPalabra;
    nuevaPalabra = malloc(sizeof(wchar_t)*largoPalabra);
    for (int i = 0; i < largoPalabra; i++){
        nuevaPalabra[i] = wtoupper(palabra[i]);
    }

    return nuevaPalabra;
}

wchar_t wtoupper (wchar_t letra){
    if (letra >= 97 && letra <= 122 || letra >= 225){
        return (letra - 32);
    } else {
        return letra;
    }
}

wchar_t wtolower (wchar_t letra){
    if (letra >= 65 && letra <= 90 || letra <= 220 && letra >= 193){
        return (letra + 32);
    } else {
        return letra;
    }
}


void separar_palabra (wchar_t palabra[]){
    int largoPalabra, cantidadSeparadas, filaMatriz = 0, contador, posPalabra;
    largoPalabra = wcslen(palabra);
    cantidadSeparadas = (largoPalabra - 1) * 2;
    wchar_t separadas[cantidadSeparadas][15];
    for (int posEspacio = 1; posEspacio < largoPalabra; posEspacio++){
        contador = 0;
        for (posPalabra = 0; posPalabra < largoPalabra; posPalabra++){
            if (posPalabra == posEspacio){
                separadas[filaMatriz][contador] = '\0';
                filaMatriz++;
                contador = 0;
                separadas[filaMatriz][contador] = palabra[posPalabra];
                contador++;
            } else {
                separadas[filaMatriz][contador] = palabra[posPalabra];
                contador++;
            }
        }
        separadas[filaMatriz][contador] = '\0';
        filaMatriz++;
    }
    for (int i = 0; i < cantidadSeparadas; i++){
        wprintf(L"%ls\n", separadas[i]);
    }
}

void intercambio (wchar_t palabra[]){
    int largoPalabra;
    wchar_t aux;
    largoPalabra = wcslen(palabra);
    for (int i = 1; i < largoPalabra; i++){
        aux = palabra[i];
        palabra[i] = palabra[i-1];
        palabra[i-1] = aux;
        wprintf(L"%ls\n", palabra);
        palabra[i-1] = palabra[i];
        palabra[i] = aux;
    }
}


void agregar_entre_letras (wchar_t palabra[], wchar_t alfabeto[]){  //anda
    int largoNuevaPalabra, indiceACambiar = 0, contador;
    largoNuevaPalabra = wcslen(palabra) + 1;
    wchar_t nuevaPalabra[largoNuevaPalabra+1]; //para el terminador
    nuevaPalabra[largoNuevaPalabra] = '\0';
    while (indiceACambiar < largoNuevaPalabra){
        contador = 0;
        for (int i = 0; i < largoNuevaPalabra; i++){  //Copia las letras excepto en el indice a cambiar
            if (i != indiceACambiar){
                nuevaPalabra[i] = palabra[contador];
                contador++;
            }
        }
        for (int i = 0; alfabeto[i]; i++){
            nuevaPalabra[indiceACambiar] = alfabeto[i];
            wprintf(L"%ls\n", nuevaPalabra);
        }
        indiceACambiar++;
    }
}


void reemplazo (wchar_t palabra[], wchar_t alfabeto[]){
    int largoPalabra;
    FILE* arxivo;
    arxivo = fopen("prueba2.txt","w");
    largoPalabra = wcslen(palabra);
    wchar_t aux; //para no perder la letra que reemplazo
    for (int i = 0; i < largoPalabra; i++){
        aux = palabra[i];
        for (int j = 0; alfabeto[j]; j++){
            palabra[i] = alfabeto[j];
            fwprintf (arxivo, L"%ls\n",palabra);
        }
        palabra[i] = aux;
    }
    fclose(arxivo);
}

wchar_t* eliminacionOP(wchar_t palabra[], int indice){
    int largoPalabra = wcslen(palabra), contadorPalabra, indiceNuevaPalabra = 0;
    
    wchar_t* nuevaPalabra = malloc(sizeof(wchar_t)*largoPalabra);
    
    for (contadorPalabra = 0; contadorPalabra < largoPalabra; contadorPalabra++){
        if (contadorPalabra != indice){
            nuevaPalabra[indiceNuevaPalabra] = palabra[contadorPalabra];
            indiceNuevaPalabra++;
        }
    }

    nuevaPalabra[contadorPalabra] = '\0';
    wprintf(L"Elimina3: %ls\n", nuevaPalabra);
}

void eliminacion (wchar_t palabra[]){              //funciona
    int largoPalabra , contadorPalabra;
    largoPalabra = wcslen(palabra);
    wprintf(L"Largo palabra: %d\n", largoPalabra);
    wchar_t aux[largoPalabra];
    for (int indiceMalo = 0; indiceMalo < largoPalabra; indiceMalo++){
        contadorPalabra = 0;
        for (int indiceNuevaPalabra = 0; indiceNuevaPalabra < largoPalabra;) {
            if (contadorPalabra == indiceMalo){
                contadorPalabra++;
            }else{
                aux[indiceNuevaPalabra] = palabra[contadorPalabra];
                indiceNuevaPalabra++;
                contadorPalabra++;
            }
        }

        wprintf(L"%ls\n", aux);
    }
}

void imprimir_matriz (wchar_t matriz[][10], int filas){
  for (int i = 0; i < filas; i++){
    wprintf(L"Fila %d: %ls", i, matriz[i]);
  }
}

void separa (wchar_t linea, wchar_t matriz[][10]){
  int indice = 0, fila = 0;
  for (int i = 0; linea[i] != '\n'; i++){
    if (linea[i] != ' '){
      matriz[fila][indice] = linea[i];
      indice++;
    } else {
      if (matriz[fila][indice-1] == '!' || matriz[fila][indice-1] == '.'){
        matriz[fila][indice-1] = '\0';
        fila++;
        indice = 0;
      } else {
        matriz[fila][indice] = '\0';
        fila++;
        indice = 0;
      }
    }
  }
  imprimir_matriz (matriz, fila);
}

void parsear_palabras (wchar_t linea[]){
  if (linea[0] == '\n'){
    wprintf(L"Esta vacia");
  } else {
    wchar_t matriz[10][10];
    separa(linea, matriz);
  }
}

void lectura_entrada (char* rutaArchivo){
  FILE* archivo;
  archivo = fopen(rutaArchivo);
  wchar_t aux[50];
  while (fwscanf(archivo, L"%ls", aux)){
    parsear_palabras(aux);
  }
  fclose(archivo);
}
